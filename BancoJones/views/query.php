<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../css/style.css">
    <title>Lista de movimientos, Banco Jones</title>
</head>
<body>
<?php
session_start();
if (isset($_SESSION['cliente'])){?>
<nav class="nav">
    <a href="init.php"> <input name="reg" type="button" value="Init"></a>
    <a href="perfil.php"><input name="ins" type="button" value="Perfil"></a>
    <a href="transfer.php"><input name="out" type="button" value="Transferencias"></a>
    <a href="query.php"><input name="perf" type="button" value="Movimientos"></a>
    <a href="logout.php"><input name="tra" type="button" value="Desconectarse"></a>
</nav>
<main>
    <form action="../controller/controller.php" method="post">
        <select name="cuentas">
            <?php
            require_once('../model/CuentaModel.php');
            require_once('../model/Cliente.php');
            $accounts=getAccounts(unserialize($_SESSION['cliente'])->getDni());
            for ($i=0; $i<sizeof($accounts) ;$i++){?>
                <option ><?php echo $accounts[$i]["cuenta"] ?></option>
            <?php }?>
        </select>
        <input name="submit" type="submit" value="Seleccionar"/>
        <input name="control" type="hidden" value="query"/>
    </form>


    <?php
    if (isset($_SESSION['saldo'])) {
        echo "Saldo total " . $_SESSION['saldo'] . ' €<br/>';
    }
    if (isset($_SESSION['lista'])) {
        $movimientos=$_SESSION['lista'];
        echo '<table class="default" rules="all" frame="border">';
        echo '<tr>';
        echo '<th>Origen</th>';
        echo '<th>Destino</th>';
        echo '<th>Hora</th>';
        echo '<th>Cantidad de dinero</th>';
        echo '</tr>';
        for ($i=0;$i<count($movimientos);$i++){
            echo '<tr>';
            echo '<td>'.$movimientos[$i]['id_origen'].'</td>';
            echo '<td>'.$movimientos[$i]['id_destino'].'</td>';
            echo '<td>'.$movimientos[$i]['fecha'].'</td>';
            echo '<td>'.$movimientos[$i]['cantidad'].'</td>';
            echo '</tr>';
        }
        echo '</table>';
    }
    }else{
        header("Location: login.php");
    }?>
</main>
</body>
</html>