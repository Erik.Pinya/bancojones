<?php


class Cuenta
{
    private $cuenta;
    private $creacion;
    private $saldo;

    /**
     * Cuenta constructor.
     * @param $cuenta
     * @param $creacion
     * @param $saldo
     */
    public function __construct($cuenta, $creacion, $saldo)
    {
        $this->cuenta = $cuenta;
        $this->creacion = new \Cassandra\Date();
        $this->saldo = $saldo;
    }

    /**
     * @return mixed
     */
    public function getCuenta()
    {
        return $this->cuenta;
    }

    /**
     * @param mixed $cuenta
     */
    public function setCuenta($cuenta): void
    {
        $this->cuenta = $cuenta;
    }

    /**
     * @return \Cassandra\Date
     */
    public function getCreacion(): \Cassandra\Date
    {
        return $this->creacion;
    }

    /**
     * @param \Cassandra\Date $creacion
     */
    public function setCreacion(\Cassandra\Date $creacion): void
    {
        $this->creacion = $creacion;
    }

    /**
     * @return mixed
     */
    public function getSaldo()
    {
        return $this->saldo;
    }

    /**
     * @param mixed $saldo
     */
    public function setSaldo($saldo): void
    {
        $this->saldo = $saldo;
    }




}